$(document).ready(function(){
    
    PUERTO = "12345"
    URL_BASE = "http://localhost:" + PUERTO
    
    $( ".btn-aniadir" ).each(function(index) {
        $(this).on("click", function(){
            $.ajax({
                url: URL_BASE+"/aniadir_prod_carr", 
                type: 'POST',
                datatype: 'json',
                data: {'id_producto':$(this).data("id")},
                success: function(result){
                    $("#num_items").html(result.num_items);
                    actualizar_carrito_pantalla();
                }
            });
        });
    
    });
    
       $( ".btn-restar" ).each(function(index) {
        $(this).on("click", function(){
            $.ajax({
                url: URL_BASE+"/restar_prod_carr", 
                type: 'POST',
                datatype: 'json',
                data: {'id_producto':$(this).data("id")},
                success: function(result){
                    $("#num_items").html(result.num_items);
                    actualizar_carrito_pantalla();
                }
            });
        });
    
    });
    
       $( ".btn-eliminar" ).each(function(index) {
        $(this).on("click", function(){
            $.ajax({
                url: URL_BASE+"/eliminar_prod_carr", 
                type: 'POST',
                datatype: 'json',
                data: {'id_producto':$(this).data("id")},
                success: function(result){
                    $("#num_items").html(result.num_items);
                    actualizar_carrito_pantalla();
                }
            });
        });
    
    });
    
    function actualizar_carrito_pantalla() {
        $.ajax({
                url: URL_BASE+"/get_items", 
                type: 'POST',
                datatype: 'json',
                success: function(data){
                    var html = "";
                    total = 0;
                    for (var i=0; i<data.items.length;i++) {
                        total += data.items[i].subtotal
                    }
                    for (var i=0; i<data.items.length;i++) {
                        var item = data.items[i];
                        html += "<tr>";
                        html += "<td>Art. #" + item.producto_id + "</td>"
                        html += "<td>" + item.nombre_producto + "</td>"
                        html += "<td>" + item.cantidad + "</td>"
                        html += "<td>" + item.precio_iva.toFixed(2) + "</td>"
                        html += "<td>" + item.subtotal.toFixed(2) + "</td>"
                        html += "</tr>";
                    }
                    if (data.items.length>0) {
                        html += "<tr><td></td><td></td><td></td><td></td><td>" +total.toFixed(2) + "</td></tr>"
                    }
                    $('#tabla_carrito').html(html);
                }
            });
    }

});
