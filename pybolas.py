from flask import Flask
from flask import request,  session, flash,   url_for,  request,  redirect,  render_template,  jsonify
import pymysql
import os

app = Flask(__name__)

PUERTO = "12345"
USER = "root"
PASSWORD = "root"


"""Conexion a la base de datos"""
def get_conexion():
    conexion = pymysql.connect(host='127.0.0.1',
                                 user= USER,
                                 password= PASSWORD,
                                 db='pybolas',
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)
    return conexion

"""Devuelve una lista de los productos con stock"""
def get_productos():  
    connection = get_conexion()
    try:
        with connection.cursor() as cursor:
            sql = "SELECT * from producto where stock > 0"
            cursor.execute(sql)
            results = cursor.fetchall()
    finally:
        cursor.close()
        connection.close()
    return results
    
"""Crea un carrito (necesario siempre que se inicia sesión)"""
def crear_carrito():
    db = get_conexion()
    cursor = db.cursor()
    sql = "INSERT INTO carrito (usuario_id, impuesto_id) values (NULL, 1)"
    cursor.execute(sql)
    id_carrito = cursor.lastrowid
    session['id_carrito'] = id_carrito
    session['items'] = []
    session['num_items'] = 0
    cursor.close()
    db.commit()
    db.close()

def asignar_usuario_a_carrito(id_usuario,  id_carrito):
    connection = get_conexion()
    try:
        with connection.cursor() as cursor:
            sql = "UPDATE carrito SET usuario_id = " + str(id_usuario) + " WHERE id = " + str(id_carrito)
            cursor.execute(sql)
    finally:
        cursor.close()
        connection.commit()
        connection.close()
        
"""Lógica principal al abrir la web: crear carrito"""
@app.route('/')
def tienda():
    if not 'carrito' in session:
        crear_carrito()
    if  not'usuario_definido' in session:
        session['usuario_definido'] = False
    else:
        if 'id_usuario' in session:
            asignar_usuario_a_carrito(session['id_usuario'], session['id_carrito'])
    lista_productos = get_productos()
    return render_template("index.html",  productos=lista_productos)
    
# login
@app.route("/login", methods=["GET", "POST"])
def login():
    respuesta = 0
    resultado = []
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']   
        connection = get_conexion()
        try:
            with connection.cursor() as cursor:
                sql = "SELECT id, email, password, nombre, apellidos from usuario where email = '" + username + "'"
                cursor.execute(sql)
                resultado = cursor.fetchone()
                sql = "SELECT md5('" + password + "') passwordmd5 FROM DUAL"
                cursor.execute(sql)
                passwordmd5 = cursor.fetchone()
                if resultado == None:
                    respuesta = 0
                elif resultado['password'] == passwordmd5['passwordmd5']:
                    respuesta = 2
                else:
                    respuesta = 1
        finally:
            cursor.close()
            connection.close()
            if respuesta == 0:
                flash('El usuario no existe')
            elif respuesta == 1:
                flash('La contraseña no es correcta')
            else:
                flash('Usuario y contraseña correctos')
                session['usuario_definido'] = True 
                session['nombre'] = resultado['nombre']
                session['apellidos'] = resultado['apellidos']
                session['id_usuario'] = resultado['id']
                #Asignar el carrito al usuario
                if 'carrito' in session:
                    asignar_usuario_a_carrito(resultado['id'], session['id_carrito'])
        return redirect(url_for('tienda'))

@app.route('/logout')
def logout():
    session.clear()
    return redirect(url_for('tienda'))
    
#Lógica carrito
def get_num_items():
    result = {'num_items': 0}
    connection = get_conexion()
    try:
        with connection.cursor() as cursor:
            sql = "SELECT coalesce(sum(cantidad),0) num_items from lineacarrito where carrito_id = " + str(session['id_carrito'])
            cursor.execute(sql)
            result = cursor.fetchone()
    finally:
        cursor.close()
        connection.close()
    return int(result['num_items'])

@app.route('/get_items', methods=["GET", "POST"])
def get_items():
    connection = get_conexion()
    try:
        with connection.cursor() as cursor:
            sql = "SELECT lc.producto_id producto_id, lc.cantidad cantidad, lc.precio * 1.21 precio_iva, "
            sql += "lc.cantidad * lc.precio * 1.21 subtotal, CONCAT(prod.modelo,' ',prod.material,' ',prod.color,' ',prod.diametro,' ', "
            sql += " (case when prod.brillo = 1 then 'brillo' else 'mate' end)) nombre_producto"
            sql += " from lineacarrito lc "
            sql += " inner join producto prod on prod.id = lc.producto_id "
            sql += " where lc.carrito_id = " + str(session['id_carrito'])
            cursor.execute(sql)
            results = cursor.fetchall()
            respuesta = {}
            items = []
            total = 0
            for result in results:
                item = {}
                item['producto_id'] = result['producto_id']
                item['cantidad'] = int(result['cantidad'])
                item['precio_iva'] = float(result['precio_iva'])
                item['subtotal'] = float(result['subtotal'])
                item['nombre_producto'] = result['nombre_producto']
                items.append(item)
                total += float(result['subtotal'])       
            respuesta['items'] = items    
    finally:
        cursor.close()
        connection.close()
    return jsonify(respuesta)

@app.route('/aniadir_prod_carr', methods=["GET", "POST"])
def aniadir_prod_carr():
    resp = modificar_carrito("aniadir", request.form['id_producto'])
    return resp
    
@app.route('/restar_prod_carr', methods=["GET", "POST"])
def restar_prod_carr():
    resp = modificar_carrito("restar", request.form['id_producto'])
    return resp
    
@app.route('/eliminar_prod_carr', methods=["GET", "POST"])
def eliminar_prod_carr():
    resp =  modificar_carrito("eliminar", request.form['id_producto'])
    return resp
    
def modificar_carrito(accion,  id_producto):
    db = get_conexion()
    cursor = db.cursor()
    #buscar si exite el producto
    sql = "SELECT id, cantidad from lineacarrito where carrito_id = " + str(session['id_carrito'])
    sql += " and producto_id = " + str(id_producto)
    cursor.execute(sql)
    results = cursor.fetchall()
    
    if accion=="aniadir":
        if len(results) > 0:
            sql = "UPDATE lineacarrito SET cantidad = cantidad + 1 where id = " + str(results[0]["id"])
            cursor.execute(sql)
        else:
            sql_precio = "SELECT precio precio from producto where id = " + str(id_producto)
            cursor.execute(sql_precio)
            result = cursor.fetchone()
            precio_producto = result["precio"]
            sql = "INSERT INTO lineacarrito (carrito_id, producto_id, cantidad, precio) values "
            sql += " (" + str(session['id_carrito']) + ", " + str(id_producto) + ", 1, " + str(precio_producto) + ")" 
            cursor.execute(sql)
    elif accion=="restar":
        if len(results) > 0:
            if int(results[0]["cantidad"]) <= 1:
                sql = "DELETE from lineacarrito where id = " + str(results[0]["id"])
                cursor.execute(sql)
            else:
                sql = "UPDATE lineacarrito SET cantidad = cantidad - 1 where id = " + str(results[0]["id"])
                cursor.execute(sql)
    else:
        if len(results) > 0:
            sql = "DELETE from lineacarrito where id = " + str(results[0]["id"])
            cursor.execute(sql)
    cursor.close()
    db.commit()
    db.close()
    num_items = {'num_items':get_num_items()}
    return jsonify(num_items)
    

#Inicialización de flask
if __name__ == "__main__":
    app.secret_key = os.urandom(24)
    app.config['SESSION_TYPE'] = 'filesystem'
    app.debug = True
    app.run(port=PUERTO)


    
