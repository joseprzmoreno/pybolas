Tienda ficticia realizada con Python y Flask.   
Autor: @joseprzmoreno     
       
Proyecto muuuy sencillo, a modo de ejercicio hecho en un finde e incompleto por ahora.   
Utiliza controlador y vistas, faltaría implementar un modelo de datos.          
       

INSTALACIÓN     
Se necesita python3 y mysql    
Instalación de módulo pymysql:    
	Pip para python3: sudo apt-get install python3-pip    
	sudo pip3 install PyMySQL    
Instalación de flask (con python 3):   
	http://flask.pocoo.org/docs/0.12/installation/    
Instalación de jinja2 (sistema de plantillas html):  
	http://jinja.pocoo.org/docs/2.9/intro/   
En pybolas.py, especificar el usuario y password de mysql y el número de puerto a utilizar para el servidor de prueba de Flask   
En bd_setup.py, especificar el usuario y password de mysql  
En main.js, especificar el número de puerto   
Ejecutar primero el archivo bd_setup.py que instalará la base de datos   
Para que funcione correctamente hay que tener conexión a Internet (por cdn jquery, imagen, etc)   
Probar en local: ejecutar pybolas.py y acceder a http://localhost:12345 (o el número de puerto que se haya especificado)   
      

Pendiente:   
-Realizar el ciclo completo de un pedido, hasta el pago.   
-Área de cliente para que el usuario pueda ver sus pedidos.   
-Realizar la vista admin para ver el estado de los pedidos.   
-Pasar la taba de productos a datatable (pagina y filtra automáticamente)   
-Registro de usuarios   
-Arreglar la continuidad del carrito cuando se inicia sesión   
-Añadir los estados del pedido en base de datos (realizado, pagado, pendiente stock, enviado, finalizado, cancelado, etc)   
-Crear alguna imagen más   
Etc.

