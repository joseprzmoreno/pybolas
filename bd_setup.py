import pymysql
import random

USUARIO = "root"
PASSWORD = "root"

#crear base de datos
db = pymysql.connect("localhost",USUARIO,PASSWORD, "")
cursor = db.cursor()
sql = 'CREATE DATABASE pybolas'
cursor.execute(sql)

#crear tablas
sql = """CREATE TABLE pybolas.usuario (
       id INT NOT NULL AUTO_INCREMENT,
       dni VARCHAR(20),
       nombre VARCHAR(100) NOT NULL,
       apellidos VARCHAR(100) NOT NULL,
       email VARCHAR(100) NOT NULL,
       password VARCHAR(100) NOT NULL,
       activo TINYINT(1) NOT NULL DEFAULT 1,
       publicidad TINYINT(1) NOT NULL DEFAULT 0,
       rol VARCHAR(100),
       fecha_creacion TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
       PRIMARY KEY (id)
       )
       """
cursor.execute(sql)

sql = """CREATE TABLE pybolas.direccion (
       id INT NOT NULL AUTO_INCREMENT,
       usuario_id INT NOT NULL,
       direccion VARCHAR(255) NOT NULL,
       cp VARCHAR(10) NOT NULL,
       localidad VARCHAR(100) NOT NULL,
       provincia VARCHAR(100) NOT NULL,
       PRIMARY KEY (id)
       )
       """
cursor.execute(sql)

sql = """ALTER TABLE pybolas.direccion
      ADD CONSTRAINT fk_direccion_usuario
      FOREIGN KEY (usuario_id) REFERENCES pybolas.usuario (id)
       """
cursor.execute(sql)


sql = """CREATE TABLE pybolas.telefono (
       id INT NOT NULL AUTO_INCREMENT,
       usuario_id INT NOT NULL,
       telefono VARCHAR(20) NOT NULL,
       PRIMARY KEY (id)
       )
       """
cursor.execute(sql)

sql = """ALTER TABLE pybolas.telefono
      ADD CONSTRAINT fk_telefono_usuario
      FOREIGN KEY (usuario_id) REFERENCES pybolas.usuario (id)
       """
cursor.execute(sql)

sql = """CREATE TABLE pybolas.tarjetacredito (
       id INT NOT NULL AUTO_INCREMENT,
       usuario_id INT NOT NULL,
       nombre VARCHAR(100),
       tipo VARCHAR(100) NOT NULL,
       ultimos_digitos VARCHAR(20) NOT NULL,
       mes_caducidad VARCHAR(10) NOT NULL,
       anio_caducidad VARCHAR(10) NOT NULL,
       codigo VARCHAR(10) NOT NULL,
       token VARCHAR(100),  
       PRIMARY KEY (id)
       )
       """
cursor.execute(sql)

sql = """ALTER TABLE pybolas.tarjetacredito
      ADD CONSTRAINT fk_tarjetacredito_usuario
      FOREIGN KEY (usuario_id) REFERENCES pybolas.usuario (id)
       """
cursor.execute(sql)

sql = """CREATE TABLE pybolas.impuesto (
       id INT NOT NULL AUTO_INCREMENT,
      nombre VARCHAR(100) NOT NULL,
      valor INT NOT NULL,
      vigente TINYINT(1) NOT NULL DEFAULT 1,
      PRIMARY KEY (id)
       )
       """
cursor.execute(sql)

sql = """CREATE TABLE pybolas.producto (
      id INT NOT NULL AUTO_INCREMENT,
      tipo VARCHAR(100) NOT NULL,
      modelo VARCHAR(100) NOT NULL,
      descripcion TEXT,
      diametro INT NOT NULL,
      material VARCHAR(100) NOT NULL,
      color VARCHAR(100) NOT NULL,
      brillo TINYINT(1) NOT NULL,
      precio DECIMAL(7,2) NOT NULL,
      stock INT NOT NULL DEFAULT 0,
      activo TINYINT(1) NOT NULL DEFAULT 1,
      url_imagen VARCHAR(100),
      observaciones TEXT,
      PRIMARY KEY (id)
       )
       """
cursor.execute(sql)

sql = """CREATE TABLE pybolas.preciostock (
      id INT NOT NULL AUTO_INCREMENT,
      producto_id INT NOT NULL,
      precio DECIMAL(7,2) NOT NULL,
      stock INT NOT NULL,
      fecha TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
      PRIMARY KEY (id)
       )
       """
cursor.execute(sql)

sql = """ALTER TABLE pybolas.preciostock
      ADD CONSTRAINT fk_preciostock_producto
      FOREIGN KEY (producto_id) REFERENCES pybolas.producto (id)
       """
cursor.execute(sql)

sql = """CREATE TABLE pybolas.promocion (
      id INT NOT NULL AUTO_INCREMENT,
      producto_id INT,
      modelo VARCHAR(100),
      diametro_min INT,
      diametro_max INT,
      material VARCHAR(100),
      color VARCHAR(100),
      provincia VARCHAR(100) DEFAULT '0',
      fecha_inicio DATETIME,
      fecha_fin DATETIME,
      valor_eur DECIMAL(7,2),
      valor_porc DECIMAL(7,2),
      PRIMARY KEY (id)
       )
       """
cursor.execute(sql)

sql = """ALTER TABLE pybolas.promocion
      ADD CONSTRAINT fk_promocion_producto
      FOREIGN KEY (producto_id) REFERENCES pybolas.producto (id)
       """
cursor.execute(sql)

sql = """CREATE TABLE pybolas.carrito (
      id INT NOT NULL AUTO_INCREMENT,
      usuario_id INT,
      fecha TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
      impuesto_id INT NOT NULL,
      PRIMARY KEY (id)
       )
       """
cursor.execute(sql)

sql = """ALTER TABLE pybolas.carrito
      ADD CONSTRAINT fk_carrito_usuario
      FOREIGN KEY (usuario_id) REFERENCES pybolas.usuario (id)
       """
cursor.execute(sql)

sql = """ALTER TABLE pybolas.carrito
      ADD CONSTRAINT fk_carrito_impuesto
      FOREIGN KEY (impuesto_id) REFERENCES pybolas.impuesto (id)
       """
cursor.execute(sql)

sql = """CREATE TABLE pybolas.lineacarrito (
      id INT NOT NULL AUTO_INCREMENT,
      carrito_id INT NOT NULL,
      producto_id INT NOT NULL,
      cantidad INT NOT NULL,
      precio DECIMAL(7,2) NOT NULL,
      PRIMARY KEY (id)
       )
       """
cursor.execute(sql)

sql = """ALTER TABLE pybolas.lineacarrito
      ADD CONSTRAINT fk_lineacarrito_carrito
      FOREIGN KEY (carrito_id) REFERENCES pybolas.carrito (id)
       """
cursor.execute(sql)

sql = """ALTER TABLE pybolas.lineacarrito
      ADD CONSTRAINT fk_lineacarrito_producto
      FOREIGN KEY (producto_id) REFERENCES pybolas.producto (id)
       """
cursor.execute(sql)

sql = """CREATE TABLE pybolas.estadopedido (
      id INT NOT NULL AUTO_INCREMENT,
      nombre INT NOT NULL,
      PRIMARY KEY (id)
       )
       """
cursor.execute(sql)

sql = """CREATE TABLE pybolas.pedido (
      id INT NOT NULL AUTO_INCREMENT,
      carrito_id INT NOT NULL,
      usuario_id INT NOT NULL,
      direccion_fact INT NOT NULL,
      direccion_envio INT NOT NULL, 
      fecha TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
      estadopedido_id INT NOT NULL,
      observaciones TEXT,
      fuente VARCHAR(100),
      PRIMARY KEY (id)
       )
       """
cursor.execute(sql)

sql = """ALTER TABLE pybolas.pedido
      ADD CONSTRAINT fk_pedido_carrito
      FOREIGN KEY (carrito_id) REFERENCES pybolas.carrito (id)
       """
cursor.execute(sql)

sql = """ALTER TABLE pybolas.pedido
      ADD CONSTRAINT fk_pedido_usuario
      FOREIGN KEY (usuario_id) REFERENCES pybolas.usuario (id)
       """
cursor.execute(sql)

sql = """ALTER TABLE pybolas.pedido
      ADD CONSTRAINT fk_pedido_direccionfact
      FOREIGN KEY (direccion_fact) REFERENCES pybolas.direccion (id)
       """
cursor.execute(sql)

sql = """ALTER TABLE pybolas.pedido
      ADD CONSTRAINT fk_pedido_direccionenvio
      FOREIGN KEY (direccion_envio) REFERENCES pybolas.direccion (id)
       """
cursor.execute(sql)

sql = """ALTER TABLE pybolas.pedido
      ADD CONSTRAINT fk_pedido_estadopedido
      FOREIGN KEY (estadopedido_id) REFERENCES pybolas.estadopedido (id)
       """
cursor.execute(sql)

sql = """CREATE TABLE pybolas.pedidohistorico (
      id INT NOT NULL AUTO_INCREMENT,
      pedido_id INT NOT NULL,
      estado_ant INT NOT NULL,
      estado_nuevo INT NOT NULL,
      fecha TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
      usuario_modificador INT NOT NULL,
      notas TEXT,
      PRIMARY KEY (id)
       )
       """
cursor.execute(sql)

sql = """ALTER TABLE pybolas.pedidohistorico
      ADD CONSTRAINT fk_pedidohistorico_usuario
      FOREIGN KEY (usuario_modificador) REFERENCES pybolas.usuario (id)
       """
cursor.execute(sql)

sql = """ALTER TABLE pybolas.pedidohistorico
      ADD CONSTRAINT fk_pedidohistorico_estadoant
      FOREIGN KEY (estado_ant) REFERENCES pybolas.estadopedido (id)
       """
cursor.execute(sql)

sql = """ALTER TABLE pybolas.pedidohistorico
      ADD CONSTRAINT fk_pedidohistorico_estadonuevo
      FOREIGN KEY (estado_nuevo) REFERENCES pybolas.estadopedido (id)
       """
cursor.execute(sql)

sql = """ALTER TABLE pybolas.pedidohistorico
      ADD CONSTRAINT fk_pedidohistorico_pedido
      FOREIGN KEY (pedido_id) REFERENCES pybolas.pedido (id)
       """
cursor.execute(sql)

sql = """CREATE TABLE pybolas.valoracion (
      id INT NOT NULL AUTO_INCREMENT,
      nombre VARCHAR(100),
      lugar VARCHAR(100),
      email VARCHAR(100),
      valoracion INT NOT NULL,
      observaciones TEXT,
      PRIMARY KEY (id)
       )
       """
cursor.execute(sql)

#insertar datos de prueba

sql = """INSERT INTO pybolas.usuario (dni, nombre, apellidos, email, password, 
              activo, publicidad, rol) values
              ('12358901G','Juan','Gómez Hurtado','jghxxxxxxx@gmail.com',md5('jg1234'),
              1,1,'cliente')
       """
cursor.execute(sql)

sql = """INSERT INTO pybolas.usuario (dni, nombre, apellidos, email, password, 
              activo, publicidad, rol) values
              ('584881023H','Rosa','León Higueras','rlhxxxxxxx@gmail.com',md5('rl1234'),
              1,1,'cliente')
       """
cursor.execute(sql)

sql = """INSERT INTO pybolas.usuario (dni, nombre, apellidos, email, password, 
              activo, publicidad, rol) values
              ('76584800V','Jaume','Solís Valls','jsvxxxxxxx@gmail.com',md5('jsv1234'),
              1,1,'cliente')
       """
cursor.execute(sql)

sql = """INSERT INTO pybolas.usuario (dni, nombre, apellidos, email, password, 
              activo, publicidad, rol) values
              ('45680133M','Alberto','Rodríguez Baquero','adminpybolas@gmail.com',md5('admin1234'),
              1,1,'administrador')
       """
cursor.execute(sql)

sql = """INSERT INTO pybolas.impuesto (nombre, valor, vigente) values
              ('IVA',21,1)
       """
cursor.execute(sql)

sql = """INSERT INTO pybolas.impuesto (nombre, valor, vigente) values
              ('IGIC',7,1)
       """
cursor.execute(sql)

colores = ["rojo", "amarillo", "azul", "verde"]
diametros = [50, 100, 150]
materiales = ["madera", "goma", "cristal"]
brillos = [0, 1]


for color in colores:
    for diametro in diametros:
        for material in materiales:
            for brillo in brillos:
                precio = float(random.randint(3000, 9000) / 100)
                stock = random.randint(1, 60)
                sql = "INSERT INTO pybolas.producto (tipo, modelo, descripcion, diametro, material, color, brillo, precio, stock, activo, url_imagen, observaciones) values "
                sql += "('bola','Estándar','En progreso', " +str(diametro) + ", '" + material + "','" + color + "'," + str(brillo) + ", " + str(precio) + ", " + str(stock) + ", 1, 'https://www.dropbox.com/s/4y4fefyt0lcc9w6/bola.png?raw=1', '') "
                cursor.execute(sql)
cursor.close()
db.commit()
db.close()

print("terminado")
